package in.co.vineet.antahkaran.db;

import org.junit.Test;


public class MongoConnectorTest {

    MongoConnector mongoConnector;
    String url = "localhost";


    @Test
    public void constructor_shouldSetMongoConnectionUrl() throws Exception {
        mongoConnector = new MongoConnector(url);

        assert mongoConnector.getUrl().equals(url);

    }
}
